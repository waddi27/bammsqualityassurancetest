package pagesproject;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ECommerceSignUpTestPageJavaBamms {
	
	WebDriver driver = null;
	
	By LoginPage = By.xpath("//a[@class='login']");
	
	By createAccountEmail = By.xpath("//input[@id='email_create']");
	By buttonCreateAccount = By.id("SubmitCreate");
	By firstnameField = By.name("customer_firstname");
	By lastnameField = By.name("customer_lastname");
	By passField = By.xpath("//input[@id='passwd']");
	
	// Your Address Form //
	By firstname = By.xpath("//input[@id='firstname']");
	By lastname = By.xpath("//input[@id='lastname']");
	By company = By.xpath("//input[@id='company']");
	By address = By.xpath("//input[@id='address1']");
	By address2 = By.xpath("//input[@id='address2']");
	By city = By.xpath("//input[@id='city']");
	By state = By.xpath("//select[@id='id_state']");
	By zip = By.xpath("//input[@id='postcode']");
	By country = By.xpath("//select[@id='id_country']");
	By additionalInformation = By.xpath("//textarea[@id='other']");
	By homePhone = By.xpath("//input[@id='phone']");
	By mobilePhone = By.xpath("//input[@id='phone_mobile']");
	By addressAlias = By.xpath("//input[@id='alias']");
	By buttonRegister = By.name("submitAccount");
	
	public ECommerceSignUpTestPageJavaBamms(WebDriver driver) {
		this.driver = driver;
	}
	
	public void loginPage() {
		driver.findElement(LoginPage).sendKeys(Keys.RETURN);
	}
	
	public void inputEmailRegister(String text) {
		driver.findElement(createAccountEmail).sendKeys(text);
	}
	
	public void buttonSignUp() {
		driver.findElement(buttonCreateAccount).sendKeys(Keys.RETURN);
	}
	
	public void checkpoint() {
		
		WebDriverWait wait = new WebDriverWait(driver, 2);
//		WebElement selectTitleUserMr = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("id_gender1")));
//		selectTitleUserMr.click();
		
		WebElement selectTitleUserMrs = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("id_gender2")));
		selectTitleUserMrs.click();
		
		WebElement selectofferFromOurPatner = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("optin")));
		selectofferFromOurPatner.click();
		
		WebElement selectOurNewsletter = wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("newsletter")));
		selectOurNewsletter.click();
	}
	
	public void firstnameUser(String text) {
		driver.findElement(firstnameField).sendKeys(text);
	}
	
	public void lastnameUser(String text) {
		driver.findElement(lastnameField).sendKeys(text);
	}
	
	public void passwordUser(String text) {
		driver.findElement(passField).sendKeys(text);
	}
	
	public void dateOfBirthUser() {
		Select dayBirth = new Select(driver.findElement(By.name("days")));
		dayBirth.selectByValue("27");
		
		Select monthBirth = new Select(driver.findElement(By.name("months")));
		monthBirth.selectByValue("8");
		
		Select yearBirth = new Select(driver.findElement(By.name("years")));
		yearBirth.selectByValue("1995");
	}
	
	public void firstNameUserLocation(String text) {
		driver.findElement(firstname).sendKeys(text);
	}
	
	public void lastNameUserLocation(String text) {
		driver.findElement(lastname).sendKeys(text);
	}
	
	public void companyName(String text) {
		driver.findElement(company).sendKeys(text);
	}
	
	public void firstAddressUser(String text) {
		driver.findElement(address).sendKeys(text);
	}
	
	public void secondAddressUser(String text) {
		driver.findElement(address2).sendKeys(text);
	}
	
	public void cityUser(String text) {
		driver.findElement(city).sendKeys(text);
	}
	
	public void stateUser() {
		Select stateUserLocation = new Select(driver.findElement(By.name("id_state")));
		stateUserLocation.selectByValue("28");
	}
	
	public void postalCodeUser(String text) {
		driver.findElement(zip).sendKeys(text);
	}
	
	public void countryUser() {
		Select countryUserLocation = new Select(driver.findElement(By.name("id_country")));
		countryUserLocation.selectByValue("21");
	}
	
	public void additionalInformationUser(String text) {
		driver.findElement(additionalInformation).sendKeys(text);
	}
	
	public void homePhoneUser(String text) {
		driver.findElement(homePhone).sendKeys(text);
	}
	
	public void mobilePhoneUser(String text) {
		driver.findElement(mobilePhone).sendKeys(text);
	}
	
	public void addressAliasUser(String text) {
		driver.findElement(addressAlias).sendKeys(text);
	}
	
	public void buttonRegisterAccount() {
		driver.findElement(buttonRegister).sendKeys(Keys.RETURN);
	}
}
