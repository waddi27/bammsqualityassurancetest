package pagesproject;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ECommerceTestPageJavaBammsMainTest {
	
	WebDriver driver = null;
	
	By LoginPage = By.xpath("//a[@class='login']");
	
	// Login Prosedure //
	By emailUser = By.xpath("//input[@id='email']");
	By passUser = By.id("passwd");
	By buttonLogin = By.name("SubmitLogin");
	
	// HomePage //
	By buttonHome = By.xpath("//ul[@class='footer_links clearfix']//a[@class='btn btn-default button button-small']");
	By buttonPopular = By.xpath("//a[@class='homefeatured']");
	By buttonBestseller = By.xpath("//a[@class='blockbestsellers']");
	By addItemOne = By.xpath("//ul[@id='homefeatured']//li[@class='ajax_block_product col-xs-12 col-sm-4 col-md-3 first-in-line first-item-of-tablet-line first-item-of-mobile-line']//span[contains(text(),'Add to cart')]");
	By addItemTwo = By.xpath("//ul[@id='homefeatured']//li[@class='ajax_block_product col-xs-12 col-sm-4 col-md-3 last-item-of-mobile-line']//span[contains(text(),'Add to cart')]");
	By addItemThree = By.xpath("//ul[@id='homefeatured']//li[@class='ajax_block_product col-xs-12 col-sm-4 col-md-3 last-item-of-tablet-line first-item-of-mobile-line']//span[contains(text(),'Add to cart')]");
	By moreDetailItemOne = By.xpath("//ul[@id='homefeatured']//li[@class='ajax_block_product col-xs-12 col-sm-4 col-md-3 first-in-line first-item-of-tablet-line first-item-of-mobile-line']//span[contains(text(),'More')]");
	By moreDetailItemTwo = By.xpath("//ul[@id='homefeatured']//li[@class='ajax_block_product col-xs-12 col-sm-4 col-md-3 last-item-of-mobile-line']//span[contains(text(),'More')]");
	By moreDetailItemThree = By.xpath("//ul[@id='homefeatured']//li[@class='ajax_block_product col-xs-12 col-sm-4 col-md-3 last-item-of-tablet-line first-item-of-mobile-line']//span[contains(text(),'More')]");
	By buttonWomenPage = By.xpath("//a[@class='sf-with-ul'][contains(text(),'Women')]");
	By buttonDressPage = By.xpath("//body[@id='index']/div[@id='page']/div[@class='header-container']/header[@id='header']/div/div[@class='container']/div[@class='row']/div[@id='block_top_menu']/ul[@class='sf-menu clearfix menu-content sf-js-enabled sf-arrows']/li[2]/a[1]");
	By buttonTshirtPage = By.xpath("//body[@id='index']/div[@id='page']/div[@class='header-container']/header[@id='header']/div/div[@class='container']/div[@class='row']/div[@id='block_top_menu']/ul[@class='sf-menu clearfix menu-content sf-js-enabled sf-arrows']/li[3]/a[1]");
	
	// WomenPage //
	By buttonTops = By.xpath("//div[@id='subcategories']//li[1]");
	By buttonDresses = By.xpath("//div[@id='subcategories']//li[2]");
	By addItemOneWomen = By.xpath("//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line first-item-of-tablet-line first-item-of-mobile-line']//span[contains(text(),'Add to cart')]");
	By addItemTwoWomen = By.xpath("//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line']//span[contains(text(),'Add to cart')]");
	By addItemThreeWomen = By.xpath("//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-in-line first-item-of-tablet-line last-item-of-mobile-line hovered']//span[contains(text(),'Add to cart')]");
	By moreDetailItemOneWomen = By.xpath("//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line first-item-of-tablet-line first-item-of-mobile-line']//span[contains(text(),'More')]");
	By moreDetailItemTwoWomen = By.xpath("//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line']//span[contains(text(),'More')]");
	By moreDetailItemThreeWomen = By.xpath("//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-in-line first-item-of-tablet-line last-item-of-mobile-line']//span[contains(text(),'More')]");
	
	// Searching Item //
	By searchItem = By.xpath("//input[@id='search_query_top']");
	By buttonSearchItem = By.xpath("//button[@name='submit_search']");
	By addItemFromSearching = By.xpath("//a[@class='button ajax_add_to_cart_button btn btn-default']");
	By checkoutItemFromSearching = By.xpath("//a[@class='btn btn-default button button-medium']");
	
	// Checkout //
	By buttonCheckoutStepOne = By.xpath("//a[@class='button btn btn-default standard-checkout button-medium']//span[contains(text(),'Proceed to checkout')]");
	By commentCheckoutStepTwo = By.xpath("//textarea[@name='message']");
	By buttonCheckoutStepTwo = By.xpath("//button[@name='processAddress']//span[contains(text(),'Proceed to checkout')]");
	By checkboxStep3 = By.id("cgv");
	By buttonCheckoutStepThree = By.xpath("//button[@name='processCarrier']//span[contains(text(),'Proceed to checkout')]");
	By payByBankWire = By.xpath("//a[@class='bankwire']");
	By payByCheck = By.xpath("//a[@class='cheque']");
	By closeOrder = By.xpath("//button[@class='button btn btn-default button-medium']");
	
	// Colour Categories //
	By beige = By.id("layered_id_attribute_group_7");
	By white = By.id("layered_id_attribute_group_8");
	By black = By.id("layered_id_attribute_group_11");
	By orange = By.id("layered_id_attribute_group_13");
	By blue = By.id("layered_id_attribute_group_14");
	By green = By.id("layered_id_attribute_group_15");
	By yellow = By.id("layered_id_attribute_group_16");
	By pink = By.id("layered_id_attribute_group_24");
	
	// -- Catalog -- //
	
	// Categories //
	By topCategories = By.id("layered_category_4");
	By dressesCategories = By.id("layered_category_8");
	
	// Size //
	By selectSizeSmall = By.id("layered_id_attribute_group_1");
	By selectSizeMedium = By.id("layered_id_attribute_group_2");
	By selectSizeLong = By.id("layered_id_attribute_group_3");
	
	// Compositions //
	By selectCotton = By.id("layered_id_feature_5");
	By selectPolyster = By.id("layered_id_feature_1");
	By selectVincose = By.id("layered_id_feature_3");
	
	// Styles //
	By selectCasual = By.id("layered_id_feature_11");
	By selectDressy = By.id("layered_id_feature_16");
	By selectGirly = By.id("layered_id_feature_13");
	
	// Properties //
	By selectCD = By.id("layered_id_feature_18");
	By selectMD = By.id("layered_id_feature_21");
	By selectMiD = By.id("layered_id_feature_20");
	By selectSD = By.id("layered_id_feature_19");
	By selectSS = By.id("layered_id_feature_17");
	
	// Availability //
	By selectInStock = By.id("layered_quantity_1");
	
	// Manufacturer //
	By selectManufacturer = By.id("layered_manufacturer_1");
	
	// Condition //
	By selectCondition = By.id("layered_condition_new");
	
	// Logout //
	By logout = By.xpath("//a[@class='logout']");
	
	
	public ECommerceTestPageJavaBammsMainTest(WebDriver driver) {
		this.driver = driver;
	}
	
	public void loginPage() {
		driver.findElement(LoginPage).sendKeys(Keys.RETURN);
	}
	
	public void inputEmailUser(String text) {
		driver.findElement(emailUser).sendKeys(text);
	}
	
	public void inputPassUser(String text) {
		driver.findElement(passUser).sendKeys(text);
	}
	
	public void buttonSignIn() {
		driver.findElement(buttonLogin).sendKeys(Keys.RETURN);
	}
	
	public void accessHome() {
		driver.findElement(buttonHome).click();
	}
	
	public void accessWomen() {
		driver.findElement(buttonWomenPage).click();
	}
	
	public void searchingItem(String text) {
		driver.findElement(searchItem).sendKeys(text);
	}
	
	public void buttonSearchingItem() {
		driver.findElement(buttonSearchItem).click();
	}
	
	public void addItemAfterSearch() {
		driver.findElement(addItemFromSearching).click();
		WebDriverWait wait = new WebDriverWait(driver, 2);
		wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='btn btn-default button button-medium']")));
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[@class='btn btn-default button button-medium']")));
		driver.findElement(By.xpath("//a[@class='btn btn-default button button-medium']")).click();
	}
	
	public void catalogFilter() {
		// Categories //
		driver.findElement(topCategories).click();
//		driver.findElement(dressesCategories).click();
		
		// Size //
		driver.findElement(selectSizeSmall).click();
//		driver.findElement(selectSizeMedium).click();
//		driver.findElement(selectSizeLong).click();
		
		// Compositions //
		driver.findElement(selectCotton).click();
//		driver.findElement(selectPolyster).click();
//		driver.findElement(selectVincose).click();
		
		// Styles //
		driver.findElement(selectCasual).click();
//		driver.findElement(selectDressy).click();
//		driver.findElement(selectGirly).click();
		
		// Properties //
		driver.findElement(selectCD).click();
//		driver.findElement(selectMD).click();
//		driver.findElement(selectMiD).click();
//		driver.findElement(selectSD).click();
//		driver.findElement(selectSS).click();
		
		// Properties //
		driver.findElement(selectInStock).click();
		
		// Manufacturer //
		driver.findElement(selectManufacturer).click();
		
		// Condition //
		driver.findElement(selectCondition).click();
	}
	
	public void colourFilter() {
		driver.findElement(beige).sendKeys(Keys.RETURN);
		driver.findElement(white).sendKeys(Keys.RETURN);
		driver.findElement(black).sendKeys(Keys.RETURN);
		driver.findElement(orange).sendKeys(Keys.RETURN);
		driver.findElement(blue).sendKeys(Keys.RETURN);
		driver.findElement(yellow).sendKeys(Keys.RETURN);
		driver.findElement(pink).sendKeys(Keys.RETURN);
	}
	
	public void sortingItem() {
//		Select lowestFirst = new Select(driver.findElement(By.id("selectProductSort")));
//		lowestFirst.selectByValue("price:asc");
		
		Select highestFirst = new Select(driver.findElement(By.id("selectProductSort")));
		highestFirst.selectByValue("price:desc");
		
//		Select aToZ = new Select(driver.findElement(By.id("selectProductSort")));
//		aToZ.selectByValue("name:asc");
		
//		Select zToA = new Select(driver.findElement(By.id("selectProductSort")));
//		zToA.selectByValue("name:desc");
		
//		Select quantity = new Select(driver.findElement(By.id("selectProductSort")));
//		quantity.selectByValue("quantity:desc");
		
//		Select referenceLowest = new Select(driver.findElement(By.id("selectProductSort")));
//		referenceLowest.selectByValue("reference:asc");
		
//		Select referenceHighest = new Select(driver.findElement(By.id("selectProductSort")));
//		referenceHighest.selectByValue("reference:desc");
	}
	
	public void addItemCartWomenPage() {
		WebDriverWait wait = new WebDriverWait(driver, 2);

//		driver.findElement(addItemOneWomen).click();

		WebElement addItemOneWomen = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 first-in-line first-item-of-tablet-line first-item-of-mobile-line']//span[contains(text(),'Add to cart')]")));
		addItemOneWomen.click();
		
		WebElement checkoutShoppingItemOne = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//a[@class='btn btn-default button button-medium']")));
		checkoutShoppingItemOne.click();
		
//		WebElement addItemTwoWomen = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//li[@class='ajax_block_product col-xs-12 col-sm-6 col-md-4 last-item-of-tablet-line']//span[contains(text(),'Add to cart')]")));
//		addItemTwoWomen.click();
		
//		WebElement continueShoppingItemTwo = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='continue btn btn-default button exclusive-medium']//span[1]")));
//		continueShoppingItemTwo.click();
		
//		driver.findElement(addItemThreeWomen).click();
//		
//		WebElement continueShoppingItemThree = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//span[@class='continue btn btn-default button exclusive-medium']//span[1]")));
//		continueShoppingItemThree.click();
	}
	
	public void checkoutStepOne() {
		driver.findElement(buttonCheckoutStepOne).click();
	}
	
	public void commentStepTwo(String text) {
		driver.findElement(commentCheckoutStepTwo).sendKeys(text);;
	}
	
	public void checkoutStepTwo() {
		driver.findElement(buttonCheckoutStepTwo).click();
	}
	
	public void termOfService() {
		driver.findElement(buttonCheckoutStepOne).click();
	}
	
	public void checkboxCheckoutStepThree() {
		driver.findElement(checkboxStep3).click();
	}
	
	public void checkoutStepThree() {
		driver.findElement(buttonCheckoutStepThree).click();
	}
	
	public void checkoutStepFourBankWire() {
		driver.findElement(payByBankWire).click();
		driver.findElement(closeOrder).click();
	}
	
	public void checkoutStepFourCheck() {
		driver.findElement(payByCheck).click();
		driver.findElement(closeOrder).click();
	}
	
	public void logoutProses() {
		driver.findElement(logout).click();
	}
	public void moreItemWomenPage() {
		driver.findElement(moreDetailItemOneWomen).sendKeys(Keys.RETURN);
		driver.findElement(moreDetailItemTwoWomen).sendKeys(Keys.RETURN);
		driver.findElement(moreDetailItemThreeWomen).sendKeys(Keys.RETURN);
	}
}
