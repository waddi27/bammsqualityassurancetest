package testproject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pagesproject.ECommerceSignUpTestPageJavaBamms;
import pagesproject.ECommerceTestPageJavaBammsMainTest;

public class ECommerceBammsTestPageMainTest {
	
	public static WebDriver driver = null;
	
	@BeforeTest
	public void setUpTest( ) {
		String projectPath = System.getProperty("user.dir");
		System.out.println(projectPath);
		System.setProperty("webdriver.chrome.driver", projectPath+"/drivers/chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
	@Test //Positif Test Case
	public static void loginTest() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
	}
	
//	@Test //Negative Test Case
	public static void loginTestWithoutPassEmailUser() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
	}
	
//	@Test //Negative Test Case
	public static void loginTestWithWrongEmailUser() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana2222222@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
	}
	
//	@Test //Negative Test Case
	public static void loginTestWithWrongPassUser() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212wa");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
	}
	
//	@Test //Negative Test Case
	public static void loginTestJustWithEmailUser() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
	}
	
//	@Test //Negative Test Case
	public static void loginTestJustWithPassUser() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
	}
	
//	@Test //Positif Test Case
	public static void accessHomePage() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
	}
	
//	@Test //Positif Test Case
	public static void accessWomenPage() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
	}
	
//	@Test //Positif Test Case
	public static void searchingProduct() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
		eCommerceTestPageJavaBammsMainTest.searchingItem("Faded Short Sleeve T-shirts");
		eCommerceTestPageJavaBammsMainTest.buttonSearchingItem();
	}
	
//	@Test //Positif Test Case
	public static void sortingItemWomenPage() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
		eCommerceTestPageJavaBammsMainTest.sortingItem();
	}
	
//	@Test //Positif Test Case
	public static void filterItemWomenPage() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
		eCommerceTestPageJavaBammsMainTest.catalogFilter();
	}
	
//	@Test //Positif Test Case
	public static void addItemToCartWomenPage() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
		eCommerceTestPageJavaBammsMainTest.catalogFilter();
		eCommerceTestPageJavaBammsMainTest.addItemCartWomenPage();
	}
	
//	@Test //Positif Test Case
	public static void checkoutStepOneItemInCart() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
		eCommerceTestPageJavaBammsMainTest.catalogFilter();
		eCommerceTestPageJavaBammsMainTest.addItemCartWomenPage();
		eCommerceTestPageJavaBammsMainTest.checkoutStepOne();
	}
	
//	@Test //Positif Test Case
	public static void checkoutStepTwoItemInCart() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
		eCommerceTestPageJavaBammsMainTest.catalogFilter();
		eCommerceTestPageJavaBammsMainTest.addItemCartWomenPage();
		eCommerceTestPageJavaBammsMainTest.checkoutStepOne();
		eCommerceTestPageJavaBammsMainTest.commentStepTwo("Tolong diperbaiki lagi layanannya");
		eCommerceTestPageJavaBammsMainTest.checkoutStepTwo();
	}
	
//	@Test //Positif Test Case
	public static void checkoutStepThreeItemInCart() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
		eCommerceTestPageJavaBammsMainTest.catalogFilter();
		eCommerceTestPageJavaBammsMainTest.addItemCartWomenPage();
		eCommerceTestPageJavaBammsMainTest.checkoutStepOne();
		eCommerceTestPageJavaBammsMainTest.commentStepTwo("Tolong diperbaiki lagi layanannya");
		eCommerceTestPageJavaBammsMainTest.checkoutStepTwo();
		eCommerceTestPageJavaBammsMainTest.checkboxCheckoutStepThree();
		eCommerceTestPageJavaBammsMainTest.checkoutStepThree();
	}
	
//	@Test //Positif Test Case
	public static void checkoutStepFourPaymentBankWire() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
		eCommerceTestPageJavaBammsMainTest.catalogFilter();
		eCommerceTestPageJavaBammsMainTest.addItemCartWomenPage();
		eCommerceTestPageJavaBammsMainTest.checkoutStepOne();
		eCommerceTestPageJavaBammsMainTest.commentStepTwo("Tolong diperbaiki lagi layanannya");
		eCommerceTestPageJavaBammsMainTest.checkoutStepTwo();
		eCommerceTestPageJavaBammsMainTest.checkboxCheckoutStepThree();
		eCommerceTestPageJavaBammsMainTest.checkoutStepThree();
		eCommerceTestPageJavaBammsMainTest.checkoutStepFourBankWire();
		eCommerceTestPageJavaBammsMainTest.logoutProses();
	}
	
//	@Test //Positif Test Case
	public static void checkoutStepFourPaymentCheck() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
		eCommerceTestPageJavaBammsMainTest.catalogFilter();
		eCommerceTestPageJavaBammsMainTest.addItemCartWomenPage();
		eCommerceTestPageJavaBammsMainTest.checkoutStepOne();
		eCommerceTestPageJavaBammsMainTest.commentStepTwo("Tolong diperbaiki lagi layanannya");
		eCommerceTestPageJavaBammsMainTest.checkoutStepTwo();
		eCommerceTestPageJavaBammsMainTest.checkboxCheckoutStepThree();
		eCommerceTestPageJavaBammsMainTest.checkoutStepThree();
		eCommerceTestPageJavaBammsMainTest.checkoutStepFourCheck();
		eCommerceTestPageJavaBammsMainTest.logoutProses();
	}
	
//	@Test //Positif Test Case
	public static void checkoutItemWithSearchingItem() {
		ECommerceTestPageJavaBammsMainTest eCommerceTestPageJavaBammsMainTest = new ECommerceTestPageJavaBammsMainTest(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBammsMainTest.loginPage();
		eCommerceTestPageJavaBammsMainTest.inputEmailUser("nana@gmail.com");
		eCommerceTestPageJavaBammsMainTest.inputPassUser("121212");
		eCommerceTestPageJavaBammsMainTest.buttonSignIn();
		eCommerceTestPageJavaBammsMainTest.accessHome();
		eCommerceTestPageJavaBammsMainTest.accessWomen();
		eCommerceTestPageJavaBammsMainTest.searchingItem("Faded Short Sleeve T-shirts");
		eCommerceTestPageJavaBammsMainTest.buttonSearchingItem();
		eCommerceTestPageJavaBammsMainTest.addItemAfterSearch();
		eCommerceTestPageJavaBammsMainTest.checkoutStepOne();
		eCommerceTestPageJavaBammsMainTest.commentStepTwo("Tolong diperbaiki lagi layanannya");
		eCommerceTestPageJavaBammsMainTest.checkoutStepTwo();
		eCommerceTestPageJavaBammsMainTest.checkboxCheckoutStepThree();
		eCommerceTestPageJavaBammsMainTest.checkoutStepThree();
//		eCommerceTestPageJavaBammsMainTest.checkoutStepFourBankWire();
		eCommerceTestPageJavaBammsMainTest.checkoutStepFourCheck();
	}
	
	@AfterTest
	public void AfterTestLogin(){
		//driver.close();
	}
}
