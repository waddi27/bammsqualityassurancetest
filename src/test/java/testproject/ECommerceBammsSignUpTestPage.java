package testproject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pagesproject.ECommerceSignUpTestPageJavaBamms;

public class ECommerceBammsSignUpTestPage {
	
	public static WebDriver driver = null;
	
	@BeforeTest
	public void setUpTest( ) {
		String projectPath = System.getProperty("user.dir");
		System.out.println(projectPath);
		System.setProperty("webdriver.chrome.driver", projectPath+"/drivers/chromedriver/chromedriver.exe");
		driver = new ChromeDriver();
	}
	
//	@Test //Positif Test Case
	public static void signUpSuccessProcess() {
		ECommerceSignUpTestPageJavaBamms eCommerceTestPageJavaBamms = new ECommerceSignUpTestPageJavaBamms(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBamms.loginPage();
		eCommerceTestPageJavaBamms.inputEmailRegister("contoh1@gmail.com");
		eCommerceTestPageJavaBamms.buttonSignUp();
		
		eCommerceTestPageJavaBamms.checkpoint();
		eCommerceTestPageJavaBamms.firstnameUser("sra");
		eCommerceTestPageJavaBamms.lastnameUser("wahyudi");
		eCommerceTestPageJavaBamms.passwordUser("123123");
		eCommerceTestPageJavaBamms.dateOfBirthUser();
		eCommerceTestPageJavaBamms.companyName("BAMMS");
		eCommerceTestPageJavaBamms.firstAddressUser("Cluster Nice A.33, Kota Deltamas");
		eCommerceTestPageJavaBamms.secondAddressUser("Jakarta");
		eCommerceTestPageJavaBamms.cityUser("Bekasi");
		eCommerceTestPageJavaBamms.stateUser();
		eCommerceTestPageJavaBamms.postalCodeUser("17530");
		eCommerceTestPageJavaBamms.countryUser();
		eCommerceTestPageJavaBamms.additionalInformationUser("Check");
		eCommerceTestPageJavaBamms.homePhoneUser("02129653300");
		eCommerceTestPageJavaBamms.mobilePhoneUser("089660103564");
		eCommerceTestPageJavaBamms.addressAliasUser("cikarang pusat");
		eCommerceTestPageJavaBamms.buttonRegisterAccount();
	}
	
//	@Test //Negatif Test Case
	public static void SignUpWithoutEmailRegister() {
		ECommerceSignUpTestPageJavaBamms eCommerceTestPageJavaBamms = new ECommerceSignUpTestPageJavaBamms(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBamms.loginPage();
		eCommerceTestPageJavaBamms.buttonSignUp();
	}
	
//	@Test //Negative Test Case
	public static void SignUpWithNotAllData() {
		ECommerceSignUpTestPageJavaBamms eCommerceTestPageJavaBamms = new ECommerceSignUpTestPageJavaBamms(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBamms.loginPage();
		eCommerceTestPageJavaBamms.inputEmailRegister("contoh2@gmail.com");
		eCommerceTestPageJavaBamms.buttonSignUp();
		eCommerceTestPageJavaBamms.checkpoint();
		eCommerceTestPageJavaBamms.firstnameUser("sra");
		eCommerceTestPageJavaBamms.lastnameUser("wahyudi");
		eCommerceTestPageJavaBamms.passwordUser("123123");
		eCommerceTestPageJavaBamms.buttonRegisterAccount();
	}
	
//	@Test //Negative Test Case
	public static void SignUpWithNotValidData() {
		ECommerceSignUpTestPageJavaBamms eCommerceTestPageJavaBamms = new ECommerceSignUpTestPageJavaBamms(driver);
		
		driver.get("http://automationpractice.com/index.php");
		
		eCommerceTestPageJavaBamms.loginPage();
		eCommerceTestPageJavaBamms.inputEmailRegister("contoh2@gmail.com");
		eCommerceTestPageJavaBamms.buttonSignUp();
		
		eCommerceTestPageJavaBamms.checkpoint();
		eCommerceTestPageJavaBamms.firstnameUser("sra");
		eCommerceTestPageJavaBamms.lastnameUser("wahyudi");
		eCommerceTestPageJavaBamms.passwordUser("123123");
		eCommerceTestPageJavaBamms.dateOfBirthUser();
		eCommerceTestPageJavaBamms.companyName("BAMMS");
		eCommerceTestPageJavaBamms.firstAddressUser("Cluster Nice A.33, Kota Deltamas");
		eCommerceTestPageJavaBamms.secondAddressUser("Jakarta");
		eCommerceTestPageJavaBamms.cityUser("Bekasi");
		eCommerceTestPageJavaBamms.stateUser();
		eCommerceTestPageJavaBamms.postalCodeUser("JawaBarat");
		eCommerceTestPageJavaBamms.countryUser();
		eCommerceTestPageJavaBamms.additionalInformationUser("Check");
		eCommerceTestPageJavaBamms.homePhoneUser("Kontak");
		eCommerceTestPageJavaBamms.mobilePhoneUser("Kontak2");
		eCommerceTestPageJavaBamms.addressAliasUser("cikarang pusat");
		eCommerceTestPageJavaBamms.buttonRegisterAccount();
	}
	
	@AfterTest
	public void AfterTestLogin(){
		//driver.close();
	}
}
